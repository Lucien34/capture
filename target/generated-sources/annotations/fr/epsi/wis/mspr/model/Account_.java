package fr.epsi.wis.mspr.model;

import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-01-14T09:05:44", comments="EclipseLink-2.7.7.v20200504-rNA")
@StaticMetamodel(Account.class)
public class Account_ { 

    public static volatile SingularAttribute<Account, Integer> idAccount;
    public static volatile SingularAttribute<Account, String> password;
    public static volatile SingularAttribute<Account, Date> createTime;
    public static volatile SingularAttribute<Account, String> email;
    public static volatile SingularAttribute<Account, String> username;
    public static volatile SingularAttribute<Account, Date> resgistrationDate;

}