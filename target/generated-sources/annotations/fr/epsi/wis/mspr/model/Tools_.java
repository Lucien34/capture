package fr.epsi.wis.mspr.model;

import fr.epsi.wis.mspr.model.Rental;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-01-14T09:05:44", comments="EclipseLink-2.7.7.v20200504-rNA")
@StaticMetamodel(Tools.class)
public class Tools_ { 

    public static volatile SingularAttribute<Tools, Integer> toolQuantity;
    public static volatile CollectionAttribute<Tools, Rental> rentalCollection;
    public static volatile SingularAttribute<Tools, Integer> idTools;
    public static volatile SingularAttribute<Tools, String> toolName;

}