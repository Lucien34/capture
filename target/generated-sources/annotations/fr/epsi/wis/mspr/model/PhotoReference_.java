package fr.epsi.wis.mspr.model;

import fr.epsi.wis.mspr.model.Account;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-01-14T09:05:44", comments="EclipseLink-2.7.7.v20200504-rNA")
@StaticMetamodel(PhotoReference.class)
public class PhotoReference_ { 

    public static volatile SingularAttribute<PhotoReference, byte[]> photo;
    public static volatile SingularAttribute<PhotoReference, Long> id;
    public static volatile SingularAttribute<PhotoReference, Account> account;

}