/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.wis.mspr.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author 33676
 */
@Entity
@Table(name = "rental")
@NamedQueries({
    @NamedQuery(name = "Rental.findAll", query = "SELECT r FROM Rental r"),
    @NamedQuery(name = "Rental.findByIdRental", query = "SELECT r FROM Rental r WHERE r.idRental = :idRental"),
    @NamedQuery(name = "Rental.findByRentDate", query = "SELECT r FROM Rental r WHERE r.rentDate = :rentDate")})
public class Rental implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_rental")
    private Integer idRental;
    @Column(name = "rent_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rentDate;
    @ManyToOne(optional = false)
    private Agent agentIdAgent;
    @JoinColumn(name = "tools_id_tools", referencedColumnName = "id_tools")
    @ManyToOne(optional = false)
    private Tools toolsIdTools;

    public Rental() {
    }

    public Rental(Integer idRental) {
        this.idRental = idRental;
    }

    public Integer getIdRental() {
        return idRental;
    }

    public void setIdRental(Integer idRental) {
        this.idRental = idRental;
    }

    public Date getRentDate() {
        return rentDate;
    }

    public void setRentDate(Date rentDate) {
        this.rentDate = rentDate;
    }

    public Agent getAgentIdAgent() {
        return agentIdAgent;
    }

    public void setAgentIdAgent(Agent agentIdAgent) {
        this.agentIdAgent = agentIdAgent;
    }

    public Tools getToolsIdTools() {
        return toolsIdTools;
    }

    public void setToolsIdTools(Tools toolsIdTools) {
        this.toolsIdTools = toolsIdTools;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRental != null ? idRental.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rental)) {
            return false;
        }
        Rental other = (Rental) object;
        if ((this.idRental == null && other.idRental != null) || (this.idRental != null && !this.idRental.equals(other.idRental))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.epsi.wis.mspr.model.Rental[ idRental=" + idRental + " ]";
    }
    
}
