/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.wis.mspr.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author 33676
 */
@Entity
@Table(name = "tools")
@NamedQueries({
    @NamedQuery(name = "Tools.findAll", query = "SELECT t FROM Tools t"),
    @NamedQuery(name = "Tools.findByIdTools", query = "SELECT t FROM Tools t WHERE t.idTools = :idTools"),
    @NamedQuery(name = "Tools.findByToolName", query = "SELECT t FROM Tools t WHERE t.toolName = :toolName"),
    @NamedQuery(name = "Tools.findByToolQuantity", query = "SELECT t FROM Tools t WHERE t.toolQuantity = :toolQuantity")})
public class Tools implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tools")
    private Integer idTools;
    @Column(name = "tool_name")
    private String toolName;
    @Column(name = "tool_quantity")
    private Integer toolQuantity;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toolsIdTools")
    private Collection<Rental> rentalCollection;

    public Tools() {
    }

    public Tools(Integer idTools) {
        this.idTools = idTools;
    }

    public Integer getIdTools() {
        return idTools;
    }

    public void setIdTools(Integer idTools) {
        this.idTools = idTools;
    }

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    public Integer getToolQuantity() {
        return toolQuantity;
    }

    public void setToolQuantity(Integer toolQuantity) {
        this.toolQuantity = toolQuantity;
    }

    public Collection<Rental> getRentalCollection() {
        return rentalCollection;
    }

    public void setRentalCollection(Collection<Rental> rentalCollection) {
        this.rentalCollection = rentalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTools != null ? idTools.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tools)) {
            return false;
        }
        Tools other = (Tools) object;
        if ((this.idTools == null && other.idTools != null) || (this.idTools != null && !this.idTools.equals(other.idTools))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.epsi.wis.mspr.model.Tools[ idTools=" + idTools + " ]";
    }
    
}
