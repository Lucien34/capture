package fr.epsi.capturewebcam;

import fr.epsi.wis.mspr.model.Agent;
import fr.epsi.wis.mspr.model.PhotoReference;
import fr.epsi.wis.mspr.model.Rental;
import fr.epsi.wis.mspr.model.Tools;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 33676
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Main m = new Main();
        m.principale();
        m.getPhoto();

    }

    public byte[] getPhoto() throws IOException {
        String path = "C:\\Users\\33676\\Documents\\NetBeansProjects\\CaptureWebcam\\capture.png";
        BufferedImage image = ImageIO.read(new File(path));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bos);
        byte[] data = bos.toByteArray();
        return data;
    }

    public void principale() throws IOException {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("fr.epsi_CaptureWebcam_jar_1.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        EntityTransaction userTransaction = em.getTransaction();
        
        //ajout d'un agent
        userTransaction.begin();
        Agent a = new Agent();
        a.setEmail("toto.tata@gmail.com");
        a.setUsername("tata");
        a.setPassword("toto");
        a.setNumAgent(102);
        em.persist(a);
        em.flush();
        userTransaction.commit();
        
        
        
        //recuperation de la liste tools
        String requete ="select * from tools";
        
        //List<Tools> tools = em.
        
        //ajout d'une location
        Rental r = new Rental();
        r.setAgentIdAgent(a);
        Tools t1 = em.find(Tools.class, 2);
        r.setToolsIdTools(t1);
        java.util.Date date = new Date(System.currentTimeMillis());
        System.out.println("Before Formatting: " + date);  
        java.sql.Time datesql = new java.sql.Time(date.getTime());
        r.setRentDate(datesql);
        
        userTransaction.begin();
        em.persist(r);
        userTransaction.commit();
         
        //enregistrement de la photo
        userTransaction.begin();
        Agent a1 = em.find(Agent.class, 1);
        PhotoReference photo = new PhotoReference();
        photo.setAccount(a1);

        photo.setPhoto(getPhoto());
        em.persist(photo);

        userTransaction.commit();

    }

}
