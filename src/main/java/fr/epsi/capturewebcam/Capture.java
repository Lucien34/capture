/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.epsi.capturewebcam;

import com.github.sarxos.webcam.Webcam;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author 33676
 */
public class Capture {

    public boolean captureWebcam() throws IOException {
        boolean bienCapture = false;

        Webcam webcam = Webcam.getDefault();

        webcam.open();
        bienCapture = ImageIO.write(webcam.getImage(), "PNG", new File("capture.png"));
        webcam.close();
        //javax.swing.ImageIcon nothing = new javax.swing.ImageIcon(ImageIO.read(getClass().getResourceAsStream("hello-world.png")));

        return bienCapture;
    }
    
    
    public void testProcedure(){
        
    }
}
